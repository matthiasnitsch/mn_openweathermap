<?php
namespace MNITSCH\T3openweathermap\Tasks;

class Fetch extends \TYPO3\CMS\Scheduler\Task\AbstractTask {


    public function execute() {

        $conf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['t3openweathermap']);

        /** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TYPO3\CMS\Extbase\Object\ObjectManager');

        /** @var \MNITSCH\T3openweathermap\Domain\Repository\LocationRepository $locationRepository */
        $locationRepository = $objectManager->get('\MNITSCH\T3openweathermap\Domain\Repository\LocationRepository');

        /** @var \MNITSCH\T3openweathermap\Domain\Repository\DataRepository $dataRepository */
        $dataRepository = $objectManager->get('\MNITSCH\T3openweathermap\Domain\Repository\DataRepository');

        $location = $locationRepository->findByUid(1);

        $url = 'http://api.openweathermap.org/data/2.5/weather?APPID=' . $conf['apiKey'];

        $fetchUrl = $url . '&id=' . $location->getCityId();

        $response = file_get_contents($fetchUrl);

        $data = new \MNITSCH\T3openweathermap\Domain\Model\Data;
        $data->setData($response);

        $dataRepository->add($data);
        $location->addWeatherData($data);

        $objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();

        //dein code mit fehlerabfragen etc.
        return TRUE; //ende gut, alles gut?
    }
}
?>