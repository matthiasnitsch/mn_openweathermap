<?php
namespace MNITSCH\T3openweathermap\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Matthias Nitsch <matthias.nitsch@me.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The location for where the weather should get fetched.
 */
class Location extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * The name of the location.
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $name = '';

	/**
	 * The latitude of the location.
	 *
	 * @var float
	 */
	protected $latitude = 0.0;

	/**
	 * The longitude of the location.
	 *
	 * @var float
	 */
	protected $longitude = 0.0;

	/**
	 * The city ID of the location.
	 *
	 * @var integer
	 */
	protected $cityId = 0;

	/**
	 * The weather data for a location.
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\MNITSCH\T3openweathermap\Domain\Model\Data>
	 * @cascade remove
	 * @lazy
	 */
	protected $weatherData = NULL;

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->weatherData = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the latitude
	 *
	 * @return float $latitude
	 */
	public function getLatitude() {
		return $this->latitude;
	}

	/**
	 * Sets the latitude
	 *
	 * @param float $latitude
	 * @return void
	 */
	public function setLatitude($latitude) {
		$this->latitude = $latitude;
	}

	/**
	 * Returns the longitude
	 *
	 * @return float $longitude
	 */
	public function getLongitude() {
		return $this->longitude;
	}

	/**
	 * Sets the longitude
	 *
	 * @param float $longitude
	 * @return void
	 */
	public function setLongitude($longitude) {
		$this->longitude = $longitude;
	}

	/**
	 * Returns the cityId
	 *
	 * @return integer $cityId
	 */
	public function getCityId() {
		return $this->cityId;
	}

	/**
	 * Sets the cityId
	 *
	 * @param integer $cityId
	 * @return void
	 */
	public function setCityId($cityId) {
		$this->cityId = $cityId;
	}

	/**
	 * Adds a Data
	 *
	 * @param \MNITSCH\T3openweathermap\Domain\Model\Data $weatherDatum
	 * @return void
	 */
	public function addWeatherData(\MNITSCH\T3openweathermap\Domain\Model\Data $weatherData) {
		$this->weatherData->attach($weatherData);
	}

	/**
	 * Removes a Data
	 *
	 * @param \MNITSCH\T3openweathermap\Domain\Model\Data $weatherDataToRemove The Data to be removed
	 * @return void
	 */
	public function removeWeatherData(\MNITSCH\T3openweathermap\Domain\Model\Data $weatherDataToRemove) {
		$this->weatherData->detach($weatherDataToRemove);
	}

	/**
	 * Returns the weatherData
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\MNITSCH\T3openweathermap\Domain\Model\Data> $weatherData
	 */
	public function getWeatherData() {
		return $this->weatherData;
	}

	/**
	 * Sets the weatherData
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\MNITSCH\T3openweathermap\Domain\Model\Data> $weatherData
	 * @return void
	 */
	public function setWeatherData(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $weatherData) {
		$this->weatherData = $weatherData;
	}

}