<?php
namespace MNITSCH\T3openweathermap\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Matthias Nitsch <matthias.nitsch@me.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * WeatherController
 */
class WeatherController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * dataRepository
	 *
	 * @var \MNITSCH\T3openweathermap\Domain\Repository\DataRepository
	 * @inject
	 */
	protected $dataRepository = NULL;

	/**
	 * action show
	 *
	 * @return void
	 */
	public function showAction() {
		$data = $this->dataRepository->findLast();
		$convertedData = json_decode($data->getFirst()->getData(), TRUE);

		$this->view->assignMultiple(array(
			'temp' => round($convertedData['main']['temp'] - 273.15, 0),
			'icon' => $convertedData['weather'][0]['icon'] . '.png'
		));
	}

}