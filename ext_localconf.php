<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'MNITSCH.' . $_EXTKEY,
	'Pi1',
	array(
		'Weather' => 'show',
		
	),
	// non-cacheable actions
	array(
		'Weather' => 'show',
	)
);

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['MNITSCH\\T3openweathermap\\Tasks\\Fetch'] = array(
	'extension'        => $_EXTKEY,
	'title'            => 'Fetch Weather data',
	'description'      => 'This task is doing something really important',
);
