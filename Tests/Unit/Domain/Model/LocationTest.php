<?php

namespace MNITSCH\T3openweathermap\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Matthias Nitsch <matthias.nitsch@me.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \MNITSCH\T3openweathermap\Domain\Model\Location.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Matthias Nitsch <matthias.nitsch@me.com>
 */
class LocationTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \MNITSCH\T3openweathermap\Domain\Model\Location
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = new \MNITSCH\T3openweathermap\Domain\Model\Location();
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getNameReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getName()
		);
	}

	/**
	 * @test
	 */
	public function setNameForStringSetsName() {
		$this->subject->setName('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'name',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getLatitudeReturnsInitialValueForFloat() {
		$this->assertSame(
			0.0,
			$this->subject->getLatitude()
		);
	}

	/**
	 * @test
	 */
	public function setLatitudeForFloatSetsLatitude() {
		$this->subject->setLatitude(3.14159265);

		$this->assertAttributeEquals(
			3.14159265,
			'latitude',
			$this->subject,
			'',
			0.000000001
		);
	}

	/**
	 * @test
	 */
	public function getLongitudeReturnsInitialValueForFloat() {
		$this->assertSame(
			0.0,
			$this->subject->getLongitude()
		);
	}

	/**
	 * @test
	 */
	public function setLongitudeForFloatSetsLongitude() {
		$this->subject->setLongitude(3.14159265);

		$this->assertAttributeEquals(
			3.14159265,
			'longitude',
			$this->subject,
			'',
			0.000000001
		);
	}

	/**
	 * @test
	 */
	public function getCityIdReturnsInitialValueForInteger() {
		$this->assertSame(
			0,
			$this->subject->getCityId()
		);
	}

	/**
	 * @test
	 */
	public function setCityIdForIntegerSetsCityId() {
		$this->subject->setCityId(12);

		$this->assertAttributeEquals(
			12,
			'cityId',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getWeatherDataReturnsInitialValueForData() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getWeatherData()
		);
	}

	/**
	 * @test
	 */
	public function setWeatherDataForObjectStorageContainingDataSetsWeatherData() {
		$weatherDatum = new \MNITSCH\T3openweathermap\Domain\Model\Data();
		$objectStorageHoldingExactlyOneWeatherData = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneWeatherData->attach($weatherDatum);
		$this->subject->setWeatherData($objectStorageHoldingExactlyOneWeatherData);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneWeatherData,
			'weatherData',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addWeatherDatumToObjectStorageHoldingWeatherData() {
		$weatherDatum = new \MNITSCH\T3openweathermap\Domain\Model\Data();
		$weatherDataObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$weatherDataObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($weatherDatum));
		$this->inject($this->subject, 'weatherData', $weatherDataObjectStorageMock);

		$this->subject->addWeatherDatum($weatherDatum);
	}

	/**
	 * @test
	 */
	public function removeWeatherDatumFromObjectStorageHoldingWeatherData() {
		$weatherDatum = new \MNITSCH\T3openweathermap\Domain\Model\Data();
		$weatherDataObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$weatherDataObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($weatherDatum));
		$this->inject($this->subject, 'weatherData', $weatherDataObjectStorageMock);

		$this->subject->removeWeatherDatum($weatherDatum);

	}
}
